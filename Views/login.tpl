<div class="container">
    <div class="row">
        <div class="col-md-3">
            <form class="form-horizontal" action='login' method="POST">
                <fieldset>
                    <h1></h1>
                    <div class="form-group">
                        <!-- Username -->
                        <label c for="username">Username</label>
                        <div class="controls">
                            <input type="text" id="username" name="username" placeholder="" class="form-control input-xlarge ">
                        </div>
                    </div>
                    <div class="form-group">
                        <!-- Password-->
                        <label for="password">Password</label>
                        <div class="controls">
                            <input type="password" id="password" name="password" placeholder="" class="form-control input-xlarge">
                        </div>
                    </div>
                    <div class="form-group">
                        <!-- Button -->
                        <div class="controls">
                            <button class="btn btn-success">Login</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>