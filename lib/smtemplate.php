<?php

/**
 * @file
 * Wrapper for Smarty Template Engine
 */

require_once('classes/Session.php');
require_once('smarty/Smarty.class.php');
require_once('smarty/configs/smtemplate_config.php');

class SMTemplate{

    private $_smarty;

   public  function __construct(){
        $this->_smarty = new Smarty();

        global $smtemplate_config;
        $this->_smarty->template_dir = $smtemplate_config['template_dir'];
        $this->_smarty->addTemplateDir($smtemplate_config['layouts_dir']);
        $this->_smarty->compile_dir = $smtemplate_config['compile_dir'];
        $this->_smarty->cache_dir = $smtemplate_config['cache_dir'];
    }

    public function render($template, $results = array(),$contents = null, $layout = 'page')
    {
        $this->assignResults($results);
        $this->assignContent($contents);

        $content = $this->_smarty->fetch($template . '.tpl');
        $this->_smarty->assign('__content', $content);
        $this->_smarty->assign('userid', $results);

        $session = Session::exists('user') ? Session::get('user') : false;
        $this->checkSession($session);
        $this->_smarty->display($layout . '.tpl');
    }

    public function assignContent($contents)
    {
        if($contents){
            foreach($contents as $key=>$content){
                $this->_smarty->assign( $key, $content);
            }
        }
    }

    public function checkSession($session = false, $login = 'login.tpl', $menus = 'menu.tpl')
    {
        if($session){
            $menu = $this->_smarty->fetch($menus);
            $this->_smarty->assign('__menu', $menu);
        }else{
            $this->_smarty->assign('__menu', null);
            $content = $this->_smarty->fetch($login);
            $this->_smarty->assign('__content', $content);
        }
    }

    public function assignResults($results)
    {
        if($results){
            foreach($results as $key => $result){
                $this->_smarty->assign($key, $result);
            }
        }
    }

}