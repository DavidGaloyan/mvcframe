<?php
/* Smarty version {Smarty::SMARTY_VERSION}, created on 2018-01-09 15:45:44
  from "C:\xampp\htdocs\extmedia\views\currency.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32-dev-23',
  'unifunc' => 'content_5a54d598222712_67908247',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a2c20d3c90d881409597d77c3047dcf70fbb1124' => 
    array (
      0 => 'C:\\xampp\\htdocs\\extmedia\\views\\currency.tpl',
      1 => 1515500547,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a54d598222712_67908247 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="container col-md-2 col-md-offset-4">
    <table class="table table-condensed table-sm table-striped col-md-1" id="users-table">
        <thead class="thead-dark">
        <tr>
            <th class="col-md-10">Currency</th>
            <th class="col-md-2">Remove</th>
        </tr>
        </thead>
        <tbody>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['contents']->value, 'content');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['content']->value) {
?>
            <tr>
                <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['content']->value['currency'];?>
" class="form-control" name="apt-user"></td>
                <td><button class="btn btn-danger remove-currency" value="<?php echo $_smarty_tpl->tpl_vars['content']->value['id'];?>
" >Remove</button></td>
            </tr>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        <tr>
            <form action="index.php" method="post">
                <td><input class="form-control" type="text" placeholder="New User Currency" name="new-cur"></td>
                <td><button class="btn btn-success" type="submit">Add new</button></td>
            </form>
        </tr>
        </tbody>
    </table>
</div><?php }
}
