<?php
/* Smarty version {Smarty::SMARTY_VERSION}, created on 2018-02-07 09:46:25
  from "C:\xampp\htdocs\mvc\views\login.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32-dev-23',
  'unifunc' => 'content_5a7abce15c3d42_09484055',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '98c9575591f2f5d8753b63f5653ff9c25721aa59' => 
    array (
      0 => 'C:\\xampp\\htdocs\\mvc\\views\\login.tpl',
      1 => 1517993142,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a7abce15c3d42_09484055 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="container">
    <div class="row">
        <div class="col-md-3">
            <form class="form-horizontal" action='login' method="POST">
                <fieldset>
                    <h1></h1>
                    <div class="form-group">
                        <!-- Username -->
                        <label c for="username">Username</label>
                        <div class="controls">
                            <input type="text" id="username" name="username" placeholder="" class="form-control input-xlarge ">
                        </div>
                    </div>
                    <div class="form-group">
                        <!-- Password-->
                        <label for="password">Password</label>
                        <div class="controls">
                            <input type="password" id="password" name="password" placeholder="" class="form-control input-xlarge">
                        </div>
                    </div>
                    <div class="form-group">
                        <!-- Button -->
                        <div class="controls">
                            <button class="btn btn-success">Login</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div><?php }
}
