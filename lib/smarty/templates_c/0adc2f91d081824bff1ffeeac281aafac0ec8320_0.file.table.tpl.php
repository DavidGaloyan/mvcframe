<?php
/* Smarty version {Smarty::SMARTY_VERSION}, created on 2017-11-17 12:49:57
  from "C:\Users\User\Desktop\api\views\table.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32-dev-23',
  'unifunc' => 'content_5a0edaf529bae7_90416994',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0adc2f91d081824bff1ffeeac281aafac0ec8320' => 
    array (
      0 => 'C:\\Users\\User\\Desktop\\api\\views\\table.tpl',
      1 => 1510922995,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a0edaf529bae7_90416994 (Smarty_Internal_Template $_smarty_tpl) {
?>
<table id="datatable" class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>Id</th>
            <th>Оффер</th>
            
            <th>Сайт</th>
            <th>Страны</th>
            <th>Мин. Выплата от до</th>
            <th>Алекса</th>
            <th>Категория</th>
            <th>Ссылка</th>
            

        </tr>
    </thead>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['contents']->value, 'content');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['content']->value) {
?>
        <tr>
            <td><?php echo $_smarty_tpl->tpl_vars['content']->value['id'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['content']->value['offer'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['content']->value['domain'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['content']->value['geo'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['content']->value['minmax'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['content']->value['alexa'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['content']->value['category'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['content']->value['redirect'];?>
</td>
        </tr>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

</table>

<?php }
}
