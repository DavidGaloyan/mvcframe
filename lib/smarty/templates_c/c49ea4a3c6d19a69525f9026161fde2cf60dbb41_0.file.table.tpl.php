<?php
/* Smarty version {Smarty::SMARTY_VERSION}, created on 2017-12-02 20:35:23
  from "/var/www/html/api/views/table.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32-dev-23',
  'unifunc' => 'content_5a22d64b935a24_73054868',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c49ea4a3c6d19a69525f9026161fde2cf60dbb41' => 
    array (
      0 => '/var/www/html/api/views/table.tpl',
      1 => 1512232370,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a22d64b935a24_73054868 (Smarty_Internal_Template $_smarty_tpl) {
?>
<table id="datatable" class="table table-striped table-bordered">
    <thead>
    <a class="btn btn-success" id="csvdomain">Csv Domain</a>
    <tr>
        <th>Id</th>
        <th>Оффер</th>
        <th>Сеть</th>
        <th>Сайт</th>
        <th>Страны</th>
        <th>Мин. Выплата от до</th>
        <th>Алекса</th>
        <th>Категория</th>

    </tr>
    </thead>
    <tfoot>
    <tr>
        <th></th>
        <th><input class="filtrable" type="text" placeholder="Search Оффер" name="offer"/></th>
        <th><input class="filtrable" type="text" placeholder="Search Сеть" name="name"/></th>
        <th><input class="filtrable" type="text" placeholder="Search Сайт" name="domain"/></th>
        <th><input class="filtrable" type="text" placeholder="Search Страны" name="geo"/></th>
        <th><input class="filtrable" type="text" placeholder="Search Мин. Выплата от до" name="minmax"/></th>
        <th><input class="filtrable" type="text" placeholder="Search Алекса" name="domainrank"/></th>
        <th><input class="filtrable" type="text" placeholder="Search Категория" name="category"/></th>
    </tr>
    </tfoot>
    <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['contents']->value, 'content');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['content']->value) {
?>
        <tr>
            <td><?php echo $_smarty_tpl->tpl_vars['content']->value['id'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['content']->value['offer'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['content']->value['links'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['content']->value['domain'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['content']->value['geo'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['content']->value['minmax'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['content']->value['alexa'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['content']->value['category'];?>
</td>

        </tr>
    <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

</table>



<?php }
}
