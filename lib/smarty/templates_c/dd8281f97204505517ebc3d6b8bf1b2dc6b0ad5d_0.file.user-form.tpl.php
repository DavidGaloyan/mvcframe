<?php
/* Smarty version {Smarty::SMARTY_VERSION}, created on 2017-11-09 14:44:47
  from "C:\Users\User\Desktop\smarty\views\admin\user-form.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32-dev-23',
  'unifunc' => 'content_5a0469df8a1223_53899969',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'dd8281f97204505517ebc3d6b8bf1b2dc6b0ad5d' => 
    array (
      0 => 'C:\\Users\\User\\Desktop\\smarty\\views\\admin\\user-form.tpl',
      1 => 1510238685,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a0469df8a1223_53899969 (Smarty_Internal_Template $_smarty_tpl) {
?>


<div class="main-container">
    <div class="sub-container">
        <div class="container1">
            <form class="contact" action="adm.php" method="post">
                <h3>User Form of <?php echo $_smarty_tpl->tpl_vars['username']->value;?>
</h3>
                <fieldset>
                    <div class="field">
                        <label>User ID</label>
                        <input value="<?php echo $_smarty_tpl->tpl_vars['userid']->value;?>
" placeholder="Username" name="userid" type="text"  required autofocus readonly>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="field">
                        <label>Username</label>
                        <input value="<?php echo $_smarty_tpl->tpl_vars['username']->value;?>
" name="username" type="text"  required>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="field">
                        <label for="userdisabled" class="checkbox">User disabled</label>
                        <input id="userdisabled" class="en-dis" type="checkbox" name="en-dis"  <?php if ($_smarty_tpl->tpl_vars['disabled']->value) {?>checked<?php } else { ?>''<?php }?>>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="field">
                        <label for="userapproved" class="checkbox">User approved</label>
                        <input id="userapproved" class="en-dis" type="checkbox" name="app-dis" <?php if ($_smarty_tpl->tpl_vars['approved']->value) {?>checked<?php } else { ?>''<?php }?>>
                    </div>
                </fieldset>

                <fieldset>
                    <div class="field">
                        <label for="userdelete " class="checkbox">User Delete</label>
                        <input id="userdelete" class="en-dis" type="checkbox" name="deleted" <?php if ($_smarty_tpl->tpl_vars['deleted']->value) {?>checked<?php } else { ?>''<?php }?>>
                    </div>
                </fieldset>

                <fieldset>
                    <button name="submit" type="submit" id="contact-submit" data-submit="...Sending">Update</button>
                </fieldset>
            </form>
        </div>

        <div class="container2">
            <form id="form2" class="contact" action="adm.php" method="post">
                <h3>Domains</h3>
                <input type="hidden" name="userid" value="<?php echo $_smarty_tpl->tpl_vars['userid']->value;?>
">
                <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['contents']->value, 'content');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['content']->value) {
?>
                    <fieldset>
                        <input id="<?php echo $_smarty_tpl->tpl_vars['content']->value['id'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['content']->value['domain'];?>
" name="domain" type="text" readonly>
                        <button style="background: <?php if ($_smarty_tpl->tpl_vars['content']->value['approved']) {?>red<?php } else { ?>#4CAF50<?php }?>;" class="form-btn appdis-btn" value="<?php if ($_smarty_tpl->tpl_vars['content']->value['approved']) {?>'0'<?php } else { ?>1<?php }?>"><?php if ($_smarty_tpl->tpl_vars['content']->value['approved']) {?>Disapprove<?php } else { ?>Approve<?php }?></button>
                        <button class="form-btn del-btn">Delete</button>
                    </fieldset>
                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


                <fieldset class="last-field">
                    <input type="text" name="add_domain" placeholder="Enter domain...">
                    <button type="submit" form="form2" class="add-domain">Add domain</button>
                </fieldset>

                <fieldset>
                    <a  href="?page=userstats&userid=<?php echo $_smarty_tpl->tpl_vars['userid']->value;?>
" class="view-statistics">View Statistics</a>
                </fieldset>

            </form>
        </div>
    </div>
</div>

<?php }
}
