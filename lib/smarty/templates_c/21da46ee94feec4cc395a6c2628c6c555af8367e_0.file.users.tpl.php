<?php
/* Smarty version {Smarty::SMARTY_VERSION}, created on 2017-10-28 09:05:27
  from "C:\Users\User\Desktop\smarty\views\admin\users.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32-dev-23',
  'unifunc' => 'content_59f448575dbe83_18260503',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '21da46ee94feec4cc395a6c2628c6c555af8367e' => 
    array (
      0 => 'C:\\Users\\User\\Desktop\\smarty\\views\\admin\\users.tpl',
      1 => 1509181525,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59f448575dbe83_18260503 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="content">

    <h1>Welcome to Statistics</h1>

    </table>
    <table>
        <tr class="tr-head">
            <th>UserId</th>
            <th>Username</th>
            <th>Wallet</th>
            <th>Enable/Disable</th>
            <th>Approve</th>
            <th>Delete</th>
            <th></th>
        </tr>

        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['contents']->value, 'content');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['content']->value) {
?>
            <tr>
                <td><?php echo $_smarty_tpl->tpl_vars['content']->value['userid'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['content']->value['username'];?>
</td>
                <td>0</td>
                <td>
                    <?php if ($_smarty_tpl->tpl_vars['content']->value['userdisabled']) {?>Disabled<?php } else { ?>Enabled<?php }?>
                </td>
                <td>
                    <?php if ($_smarty_tpl->tpl_vars['content']->value['userapproved']) {?>Approved<?php } else { ?>Waiting<?php }?>
                </td>
                <td>
                    <?php if ($_smarty_tpl->tpl_vars['content']->value['userdeleted']) {?>Deleted<?php } else { ?>Active<?php }?>
                </td>
                <td>
                    <a class="edit" href="?userid=<?php echo $_smarty_tpl->tpl_vars['content']->value['userid'];?>
">Edit</a>
                </td>
            </tr>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>


    </table>
</div>


<?php }
}
