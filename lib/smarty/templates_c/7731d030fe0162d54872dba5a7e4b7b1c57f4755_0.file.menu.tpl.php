<?php
/* Smarty version {Smarty::SMARTY_VERSION}, created on 2017-12-21 08:31:32
  from "C:\xampp\htdocs\extmedia\views\menu.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32-dev-23',
  'unifunc' => 'content_5a3b6354e3de33_29699076',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7731d030fe0162d54872dba5a7e4b7b1c57f4755' => 
    array (
      0 => 'C:\\xampp\\htdocs\\extmedia\\views\\menu.tpl',
      1 => 1513841492,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a3b6354e3de33_29699076 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="container col-md-12">
    <ul class="  ul-menu col-md-12">
        <li><a class="active" href="?page=home"><i class="fa fa-home fa-fw" aria-hidden="true"></i> Offers</a>
        </li>
        <li><a class="active" href="?page=add"><i class="fa fa-plus fa-fw" aria-hidden="true"></i> Add</a></li>
        <li><a class="active" href="?page=sections"><i class="fa fa-list fa-fw" aria-hidden="true"></i> Sections</a>
        </li>
        <li><a class="active" href="?page=categories"><i class="fa fa-folder-open fa-fw" aria-hidden="true"></i>
                Categories</a></li>
        <li><a class="active" href="?page=users"><i class="fa fa-users fa-fw" aria-hidden="true"></i> Users</a>
        </li>
        <li><a class="active" href="?page=currency"><i class="fa fa-usd fa-fw" aria-hidden="true"></i> Currency</a>
        </li>
        <li class="nav navbar-nav navbar-right">
            <a href="logout.php">
                <i class="fa fa-sign-out fa-fw" aria-hidden="true"></i>
                Log Out
            </a>
        </li>
    </ul>
</div>


<?php }
}
