<?php
/* Smarty version {Smarty::SMARTY_VERSION}, created on 2018-01-20 07:12:10
  from "C:\xampp\htdocs\ladminka\layouts\page.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32-dev-23',
  'unifunc' => 'content_5a62ddba200f13_31325875',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b5e9dc9ba86e5538c1ad61f753e702c84bf4abb9' => 
    array (
      0 => 'C:\\xampp\\htdocs\\ladminka\\layouts\\page.tpl',
      1 => 1516428725,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a62ddba200f13_31325875 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html >
<head>

    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>
    <?php echo '<script'; ?>

            src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"
            type="text/javascript">

    <?php echo '</script'; ?>
>

    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-1.12.4.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"><?php echo '</script'; ?>
>


    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">

    <?php echo '<script'; ?>
 src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"><?php echo '</script'; ?>
>
    <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet">

    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" title="no title" charset="utf-8" />
    <?php echo '<script'; ?>
 src="script/myscript.js"><?php echo '</script'; ?>
>

</head>

<body>
<div class="menu">
    <?php echo $_smarty_tpl->tpl_vars['__menu']->value;?>

</div>

<div class="page-content">
    <?php echo $_smarty_tpl->tpl_vars['__content']->value;?>

</div>

</body>
</html>
<?php }
}
