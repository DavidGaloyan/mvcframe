<?php
/* Smarty version {Smarty::SMARTY_VERSION}, created on 2017-12-13 15:33:41
  from "C:\xampp\htdocs\api\layouts\page.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32-dev-23',
  'unifunc' => 'content_5a313a450cb767_20109379',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd043a9e9d628377748237a9f9ab100506e595a71' => 
    array (
      0 => 'C:\\xampp\\htdocs\\api\\layouts\\page.tpl',
      1 => 1513175618,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a313a450cb767_20109379 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html >
<head>

    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" title="no title" charset="utf-8" />
    <?php echo '<script'; ?>

            src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"
            type="text/javascript">

    <?php echo '</script'; ?>
>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-1.12.4.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="script/myscript.js"><?php echo '</script'; ?>
>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">

    <?php echo '<script'; ?>
 src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"><?php echo '</script'; ?>
>
    <link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet">







</head>

<body>
<div class="menu">
    <?php echo $_smarty_tpl->tpl_vars['__menu']->value;?>

</div>

<div class="page-content">
    <?php echo $_smarty_tpl->tpl_vars['__content']->value;?>

</div>

</body>
</html>
<?php }
}
