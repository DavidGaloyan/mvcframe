<?php
/* Smarty version {Smarty::SMARTY_VERSION}, created on 2017-12-20 12:47:19
  from "C:\xampp\htdocs\extmedia\views\home.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32-dev-23',
  'unifunc' => 'content_5a3a4dc7b30387_64630656',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3e548291512bb4cc5c145e98bbcd0f7613458077' => 
    array (
      0 => 'C:\\xampp\\htdocs\\extmedia\\views\\home.tpl',
      1 => 1513770438,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a3a4dc7b30387_64630656 (Smarty_Internal_Template $_smarty_tpl) {
?>
<table id="datatable" class="table table-striped table-bordered">
    <thead>

    <tr>
        
        <th><input class="filtrable" type="text" placeholder="Search User" name="username"/></th>
        <th><input class="filtrable" type="text" placeholder="Search Domain" name="domain"/></th>
        <th><input class="filtrable" type="text" placeholder="Search Email" name="email"/></th>
        <th><input class="filtrable" type="text" placeholder="Search Balance" name="amount"/></th>
        <th><input class="filtrable" type="text" placeholder="Search Currency" name="currency"/></th>
        <th><input class="filtrable" type="hidden" placeholder="Search Section" name="name"/></th>
        <th><input class="filtrable" type="text" placeholder="Search Last Update" name="last_update"/></th>
    </tr>
    <tr>
       
        <th>User</th>
        <th>Domain</th>
        <th>Email</th>
        <th>Balance</th>
        <th>Currency</th>
        <th>Section</th>
        <th>Last Updated</th>
    </tr>

    </thead>
    <tfoot>
    <tr>
        
        <th><input class="filtrable" type="text" placeholder="Search User" name="username"/></th>
        <th><input class="filtrable" type="text" placeholder="Search Domain" name="domain"/></th>
        <th><input class="filtrable" type="text" placeholder="Search Email" name="email"/></th>
        <th><input class="filtrable" type="text" placeholder="Search Balance" name="amount"/></th>
        <th><input class="filtrable" type="text" placeholder="Search Currency" name="currency"/></th>
        <th><input class="filtrable" type="hidden" placeholder="Search Section" name="name"/></th>
        <th><input class="filtrable" type="text" placeholder="Search Last Update" name="last_update"/></th>
    </tr>
    </tfoot>
</table><?php }
}
