<?php
/* Smarty version {Smarty::SMARTY_VERSION}, created on 2018-01-09 14:24:36
  from "C:\xampp\htdocs\extmedia\views\users.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32-dev-23',
  'unifunc' => 'content_5a54c29443beb1_58529192',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c83ce2470345bf960d0d9b6d761768185b18b42b' => 
    array (
      0 => 'C:\\xampp\\htdocs\\extmedia\\views\\users.tpl',
      1 => 1515504101,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a54c29443beb1_58529192 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="container col-md-4 col-md-offset-4">
    <table class="table table-condensed table-sm table-striped col-md-1" id="users-table">
        <thead class="thead-dark">
        <tr>
            <th class="col-md-5">Login</th>
            <th class="col-md-5">Password</th>
            <th class="col-md-1">Remove</th>

        </tr>
        </thead>
        <tbody>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['contents']->value, 'content');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['content']->value) {
?>
            <tr>
                <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['content']->value['user_login'];?>
" class="form-control" name="upd-user"></td>
                <td><input type="text" placeholder="Update password" class="form-control" name="upd-pass"></td>
                <td>
                    <button class="btn btn-success update-user" value="<?php echo $_smarty_tpl->tpl_vars['content']->value['id'];?>
">Update</button>
                </td>
                <td>
                    <button class="btn btn-danger remove-user" value="<?php echo $_smarty_tpl->tpl_vars['content']->value['id'];?>
">Remove</button>
                </td>
            </tr>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        <tr>
            <form action="index.php" method="post">
                <td><input class="form-control" type="text" placeholder="New User login" name="new-user"></td>
                <td><input class="form-control" type="text" placeholder="New User password" name="new-pass"></td>
                <td><button class="btn btn-success col-md-offset-6" type="submit">Add User</button></td>
            </form>
        </tr>
        </tbody>
    </table>
</div><?php }
}
