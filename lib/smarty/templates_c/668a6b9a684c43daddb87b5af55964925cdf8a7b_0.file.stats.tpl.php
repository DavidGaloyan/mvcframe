<?php
/* Smarty version {Smarty::SMARTY_VERSION}, created on 2018-02-05 11:33:29
  from "C:\xampp\htdocs\ladminka-v2\views\stats.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32-dev-23',
  'unifunc' => 'content_5a7832f9103a69_37172286',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '668a6b9a684c43daddb87b5af55964925cdf8a7b' => 
    array (
      0 => 'C:\\xampp\\htdocs\\ladminka-v2\\views\\stats.tpl',
      1 => 1517826699,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a7832f9103a69_37172286 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="container col-md-offset-4">
    <div class='col-md-3'>
        <div class="form-group">
            <div class='input-group date' id='datetimepicker1'>
                <input id="min_int" type='text' class="form-control filtrable" name="min-int"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class="form-group">
            <div class='input-group date' id='datetimepicker2'>
                <input id="max_int" type='text' class="form-control filtrable" name="max-int"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
        </div>
    </div>
    <div class='col-md-3'>
        <div class="form-group">
            <div class='input-group date' id='datetimepicker2'>
                <button id="statsBtn" class="form-control btn btn-success disabled">Stats</button>
            </div>
        </div>
    </div>
</div>
<div class="container col-md-5 col-md-offset-4">
    <table id="stats" class="table table-striped table-bordered">
        <thead>
        <tr>
            <th>
                <input class="filtrable" type="text" placeholder="Search Site" name="site_name"/>
            </th>
            <th>
                <input class="filtrable" type="text" placeholder="Search Offer" name="offer_name"/>
            </th>
        </tr>
        <tr>
            <th>Site</th>
            <th>Offers</th>
            <th>Leads</th>
            <th>Interval</th>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <th>
                <input class="filtrable" type="text" placeholder="Search Offer" name="site"/>
            </th>
            <th><input class="filtrable" type="text" placeholder="Search Offer" name="offer_name"/></th>
        </tr>
        </tfoot>
    </table>
</div>
<?php }
}
