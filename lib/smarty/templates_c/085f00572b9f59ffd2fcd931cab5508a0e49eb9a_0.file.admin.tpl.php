<?php
/* Smarty version {Smarty::SMARTY_VERSION}, created on 2017-10-20 11:01:39
  from "C:\Users\User\Desktop\smarty\views\admin\admin.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32-dev-23',
  'unifunc' => 'content_59e9d793458088_58673998',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '085f00572b9f59ffd2fcd931cab5508a0e49eb9a' => 
    array (
      0 => 'C:\\Users\\User\\Desktop\\smarty\\views\\admin\\admin.tpl',
      1 => 1508496935,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59e9d793458088_58673998 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="content">
    <div class="header">
        <form class="dateform" action="index.php" method="get">
            <input type="text" id="datepicker" name="datepicker" placeholder="  choose a date:">
            <button class="date-submit" type="submit">
                Show <i class="fa fa-search fa-fw search-icon" aria-hidden="true"></i>
            </button>
        </form>
        <h1>Welcome to Statistics</h1>
    </div>
    </table>
    <table>
        <tr class="tr-head">
            <th>Username</th>
            <th>Enable/Disable</th>
            <th>Approve</th>
        </tr>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['contents']->value, 'content');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['content']->value) {
?>
            <tr>
                <td><?php echo $_smarty_tpl->tpl_vars['content']->value['username'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['content']->value['userdisabled'];?>
</td>
                <td><?php echo $_smarty_tpl->tpl_vars['content']->value['userapproved'];?>
</td>
            </tr>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    </table>
</div><?php }
}
