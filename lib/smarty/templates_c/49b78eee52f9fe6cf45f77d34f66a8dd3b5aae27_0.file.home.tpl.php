<?php
/* Smarty version {Smarty::SMARTY_VERSION}, created on 2017-10-11 06:25:07
  from "C:\Users\User\Desktop\smarty\views\home.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32-dev-23',
  'unifunc' => 'content_59ddb943ce73a3_54757136',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '49b78eee52f9fe6cf45f77d34f66a8dd3b5aae27' => 
    array (
      0 => 'C:\\Users\\User\\Desktop\\smarty\\views\\home.tpl',
      1 => 1507703106,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59ddb943ce73a3_54757136 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once 'C:\\Users\\User\\Desktop\\smarty\\lib\\smarty\\plugins\\modifier.date_format.php';
?>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <title>Home</title>
    <link rel="stylesheet" href="/css/master.css" type="text/css" media="screen" title="no title" charset="utf-8" />
</head>
<body>
<p>Hello, <?php echo $_smarty_tpl->tpl_vars['receiver']->value;?>
! It's <?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['date']->value,"%d %B");?>
 today!</p>
</body>
</html><?php }
}
