<?php
/* Smarty version {Smarty::SMARTY_VERSION}, created on 2017-10-27 05:27:24
  from "C:\Users\User\Desktop\smarty\views\home.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32-dev-23',
  'unifunc' => 'content_59f2c3bc6dd840_12923162',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5e6578dc5f4f26795cbc1548eaf2ff200739b1e7' => 
    array (
      0 => 'C:\\Users\\User\\Desktop\\smarty\\views\\home.tpl',
      1 => 1509082043,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59f2c3bc6dd840_12923162 (Smarty_Internal_Template $_smarty_tpl) {
if (!is_callable('smarty_modifier_date_format')) require_once 'C:\\Users\\User\\Desktop\\smarty\\lib\\smarty\\plugins\\modifier.date_format.php';
?>


    <div class="content">
        <div class="header">
            <form class="dateform" action="index.php" method="get">
              <input type="text" id="datepicker" name="datepicker" placeholder="  choose a date:">
                <button class="date-submit" type="submit">
                   Show <i class="fa fa-search fa-fw search-icon" aria-hidden="true"></i>
                </button>
            </form>
            <h1>Welcome to Statistics</h1>
        </div>
        </table>
        <table>
            <tr class="tr-head">
                <th>Date</th>
                <th>Traffic source</th>
                <th>Domain</th>
                <th>Clicks</th>
                <th>Impressions</th>
                <th>Total clicks</th>
                <th>Total impressions</th>
            </tr>
            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['contents']->value, 'content');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['content']->value) {
?>
                    <tr>
                        <td><?php echo smarty_modifier_date_format($_smarty_tpl->tpl_vars['content']->value['statedate']);?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['content']->value['sourcename'];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['content']->value['domain'];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['content']->value['clicks'];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['content']->value['impressions'];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['content']->value['total_clicks'];?>
</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['content']->value['total_imps'];?>
</td>
                    </tr>
            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        </table>
    </div>

<?php }
}
