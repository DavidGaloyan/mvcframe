<?php
/* Smarty version {Smarty::SMARTY_VERSION}, created on 2017-11-01 10:49:09
  from "C:\Users\User\Desktop\smarty\layouts\page.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32-dev-23',
  'unifunc' => 'content_59f9a6a5030dd7_90412757',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9f230a717b98e1a227a1ab9171cda37067403738' => 
    array (
      0 => 'C:\\Users\\User\\Desktop\\smarty\\layouts\\page.tpl',
      1 => 1509533348,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_59f9a6a5030dd7_90412757 (Smarty_Internal_Template $_smarty_tpl) {
?>
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home</title>
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen" title="no title" charset="utf-8" />
    <?php echo '<script'; ?>

            src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"
            type="text/javascript">

    <?php echo '</script'; ?>
>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab" rel="stylesheet">
    <title>jQuery UI Datepicker - Default functionality</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <?php echo '<script'; ?>
 src="https://code.jquery.com/jquery-1.12.4.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 src="script/myscript.js"><?php echo '</script'; ?>
>

</head>
<body>
        <div class="menu">
            <?php echo $_smarty_tpl->tpl_vars['__menu']->value;?>

        </div>
        <hr />
            <?php echo $_smarty_tpl->tpl_vars['__content']->value;?>

        <hr />

</body>
</html><?php }
}
