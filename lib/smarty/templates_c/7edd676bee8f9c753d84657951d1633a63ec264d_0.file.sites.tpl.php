<?php
/* Smarty version {Smarty::SMARTY_VERSION}, created on 2018-02-03 10:27:52
  from "C:\xampp\htdocs\ladminka-v2\views\sites.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.32-dev-23',
  'unifunc' => 'content_5a758098e72537_92981775',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7edd676bee8f9c753d84657951d1633a63ec264d' => 
    array (
      0 => 'C:\\xampp\\htdocs\\ladminka-v2\\views\\sites.tpl',
      1 => 1517649036,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a758098e72537_92981775 (Smarty_Internal_Template $_smarty_tpl) {
?>
<div class="container col-md-offset-3">
    <div class="col-md-8">
        <form method="post" action="index.php">
            <div class='col-md-5'>
                    <input type='text' class="form-control" placeholder="Enter Site" name="site"/>
            </div>
            <div class='col-md-5'>
                    <input type='text' class="form-control" placeholder="Enter Apikey" name="apikey"/>
            </div>
            <div class='col-md-2'>
                <div class="form-group">
                    <div class='input-group'>
                        <button type="submit" class="form-control btn btn-success disabled" disabled>Add Domain</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div class="container col-md-6 col-md-offset-3">
    <table class="table table-condensed table-sm table-striped col-md-1" id="users-table">
        <thead class="thead-dark">
        <tr>
            <th class="col-md-5">Domain</th>
            <th class="col-md-5">Apikey</th>
            <th class="col-md-1">Options</th>

        </tr>
        </thead>
        <tbody>
        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['contents']->value, 'content');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['content']->value) {
?>
            <tr>
                <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['content']->value['site_name'];?>
" class="form-control" name="update-site"></td>
                <td><input type="text" value="<?php echo $_smarty_tpl->tpl_vars['content']->value['apikey'];?>
" class="form-control" name="update-apikey"></td>
                <td>
                    <button class="btn btn-success update-site" value="<?php echo $_smarty_tpl->tpl_vars['content']->value['id'];?>
">Update</button>
                </td>
                <td>
                    <button class="btn btn-danger remove-site" value="<?php echo $_smarty_tpl->tpl_vars['content']->value['id'];?>
">Remove</button>
                </td>
            </tr>
        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

        </tbody>
    </table>
</div><?php }
}
