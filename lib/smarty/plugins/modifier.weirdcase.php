<?php


function smarty_modifier_weirdcase($string){
    $str_array = str_split($string);
    $result = '';
    $vowels = array('a', 'e', 'i', 'o', 'u');

    foreach ($str_array as $char){
        if (in_array( $char , $vowels)) $result .= strtolower($char);
        else $result .= strtoupper($char);
    }

    return $result;
}