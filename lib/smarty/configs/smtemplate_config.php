<?php

/**
* @file
* Configuration file for the SMTemplate class
*/

    $smtemplate_config =
        array(
            'layouts_dir' => 'layouts/',
            'template_dir' => 'views/',
            'compile_dir' => 'lib/smarty/templates_c/',
            'cache_dir' => 'lib/smarty/cache/',
            'configs_dir' => 'lib/smarty/configs/',
        );