<?php

require_once('lib/smtemplate.php');
require_once('classes/Route.php');
require_once 'core/init.php';

Route::get('index.php', function () {
    $page = Session::exists('user') ? 'home' : 'login';
    Route::smarty()->render($page);
});

Route::get('logout', function () {
    $_SESSION = [];
    Route::smarty()->render('login');
});

Route::get('login', function () {
    $user = new User();
    $page = Session::exists('user') ? 'home' : 'login';

    if (!empty($_REQUEST['username']) && !empty($_REQUEST['password'])) {
        $page = $user->login($_REQUEST['username'], $_REQUEST['password']) ? 'home' : 'login';
    }
    Route::smarty()->render($page);
});




