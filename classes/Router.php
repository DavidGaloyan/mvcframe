<?php
class router
{
    private $request;
    protected $routes=[];
    public function __construct()
    {
        $requestURL = $_SERVER['REQUEST_URI'];
        if ($requestURL == '/') {
            // do stuff for the homepage
        }else{
            $parts = explode('/',$requestURL);
            $controller = ($parts[1])?$parts[1]:'';
            $action = ($parts[2])?$parts[2]:'';
            $parameter = ($parts[3])?$parts[3]:'';
            if($controller){
                $classname = strtolower(trim($controller));
                $method = strtolower(trim($action));
                try{
                    $class = new $classname();
                    if(method_exists($class,$method)){
                        $parameter = trim($parameter);
                        return $class->$method($parameter);
                    }else{
                        throw new Exception ("Method `$method` not found on `$class` class");
                    }
                }catch (Exception $e){
                    echo $e;
                }
            }
        }
    }

    function my_autoloader($class) {
        $class_pice = explode('\\',$class);
        $path = implode(DIRECTORY_SEPARATOR,$class_pice);
        $file = $path.'.php';
        if(file_exists($file)){
            require $file;
        }else{
            throw new Exception("File on this path $file not found!");
        }
    }
}