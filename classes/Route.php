<?php

class Route
{
    public static $validRoutes = [];

    public static function get($route, $closure)
    {
        self::$validRoutes [] = $route;

        if($_GET['url'] == $route){
            $closure->__invoke();
        }

    }

    public static function role(array $roles)
    {
        return $roles;
    }

    public static function smarty()
    {
        return new SMTemplate();
    }

}