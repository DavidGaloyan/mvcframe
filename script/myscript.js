$(document).ready(function () {

    activeMenu();

    var table = $('#datatable').DataTable({
        "processing": true,
        "serverSide": true,
        "paging": true,
        "dom": '<"top"flp<"clear">>rt<"bottom"ifp<"clear">>',
        "leght": 25,
        "lengthMenu": [[100, 50, 25, 10], [100, 50, 25, 10]],
        ajax: {
            url: "index.php/?show=true",
            type: "GET",
            data: function (d) {
                var filters = {};
                $('.filtrable').each(function () {
                    if ($(this).val()) {
                        filters[$(this).attr('name')] = $(this).val();
                    }
                });
                d.filters = filters;
            }
        },
        "columns": [
            {"data": "domain_name"}
        ],
        "columnDefs": [
            {className: "text-left", "targets": [0]}
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            if (aData['status'] == "1") {
                $('td', nRow).css({'background-color': '#CCFF99', 'color': 'black'});
            }
            else if (aData['status'] == "2") {
                $('td', nRow).css({'background-color': '#FFFFCC', 'color': 'black'});
            }
            if (aData['blocked'] == "1") {
                $('td', nRow).css({'background-color': '#FF9999', 'color': 'white'});
            }
        }

    });

    $('.filtrable').on('change', function () {
        table.ajax.reload();
    });

    var stats = $('#stats').DataTable({
        "processing": true,
        "serverSide": true,
        "paging": true,
        "dom": '<"top"flp<"clear">>rt<"bottom"ifp<"clear">>',
        "leght": 25,
        "lengthMenu": [[100, 50, 25, 10 ], [100, 50, 25, 10]],
        ajax:{
            url: "index.php/?stats=stats",
            type: "GET",
            data: function (d) {
                var filters = {};
                $('.filtrable').each(function () {
                    if($(this).val()){
                        filters[$(this).attr('name')] = $(this).val();
                    }
                });
                d.filters = filters;
            }
        },
        "order": [[2, 'desc']],
        "columns": [
            {"data": "site_name"},
            {"data": "offer_name"},
            {"data": "leads_count"},
            {"data": "interval"}
        ],
        "columnDefs": [
            //{ className: "text-left", "targets": [ 0 ] }
            //{ "orderable": false, "targets": 2 }
        ]

    });

    $( '.filtrable').on( 'change', function () {
        stats.ajax.reload();
    });

    $("input[name='domains[]']").on('keyup', function () {

        var thisinput = $(this);
        var checkdom = $(this).val();

        $.ajax({
            url: "index.php",
            type: "POST",
            data: {checkdom: checkdom},
            success: function (response) {
                if (response == 1) {
                    thisinput.next().remove();
                    thisinput.after("<div class='alert alert-warning margin-0 padding-0'>This domain is in use</div>");
                } else if (response == 2) {
                    thisinput.next().remove();
                    thisinput.after("<div class='alert alert-danger margin-0 padding-0'>This domain has been already been used</div>");
                } else if (response == 0) {
                    thisinput.next().remove();
                    thisinput.after("<div class='alert alert-info margin-0 padding-0'>Not used but already inserted</div>");
                } else if (response == 3) {
                    thisinput.next().remove();
                }

                var values = $("input[name='domains[]']")
                    .map(function () {
                        return $(this).val();
                    }).get();
                values = values.filter(function (n) {
                    return (n);
                });

                if (values.length >= 10 && !$('div').hasClass('alert')) {
                    if (!checkIfArrayIsUnique(values)) {
                        $('.submit').attr('disabled', true);
                    } else {
                        $('.submit').attr('disabled', false);
                    }
                } else {
                    $('.submit').attr('disabled', true);
                }
            }
        });


    });

    function checkIfArrayIsUnique(myArray) {
        return myArray.length === new Set(myArray).size;
    }

    $('.remove-currency').click(function () {
        var values = {};
        values['remcurr'] = $(this).val();
        ajax(values, 'currency');

    });

    $('#blocked-dom').click(function () {

        if ($(this).val() == 1) {
            $(this).val(0);
            $(this).text('Blocked');
            $(this).removeClass('btn-success').addClass('btn-danger');
        } else {
            $(this).val(1);
            $(this).text('Not Blocked');
            $(this).removeClass('btn-danger').addClass('btn-success');
        }
        table.ajax.reload();
    });

    function activeMenu() {
        $('.ul-menu a').each(function () {
            if ($(this).attr('href') == '?page=' + urlParam('page')) {
                $('.ul-menu').find('li.active').removeClass('active');
                $(this).parents("li").find('i').addClass('active');
            }
        });
    }

    function urlParam(name) {
        var results = new RegExp('[\?&]' + name + '=([^]*)').exec(window.location.href);
        if (results == null) {
            return null;
        }
        else {
            return results[1] || 0;
        }
    }

    function ajax(values) {
        $.ajax({
            url: "index.php",
            type: "POST",
            data: values,
            success: function (response) {
            }
        })
    }

    $('a').hover(function () {
        $(this).addClass('colorOrange');
        $(this).find('i').addClass('colorBlack');
    }, function () {
        $(this).removeClass('colorOrange');
    });


    $(function () {
        $('#datetimepicker1').datetimepicker({
            format : 'DD/MM/YYYY HH:mm:ss'
        });
        $('#datetimepicker2').datetimepicker({
            format : 'DD/MM/YYYY HH:mm:ss',
            useCurrent: false
        });
        $("#datetimepicker1").on("dp.change", function (e) {
            $('#datetimepicker2').data("DateTimePicker").minDate(e.date);
            if($('#min_int').val() !== '' && $('#max_int').val() !== ''){
                $('#statsBtn').removeClass('disabled');
            }
        });
        $("#datetimepicker2").on("dp.change", function (e) {
            $('#datetimepicker1').data("DateTimePicker").maxDate(e.date);
            if($('#min_int').val() !== '' && $('#max_int').val() !== ''){
                $('#statsBtn').removeClass('disabled');
            }
        });
    });

    $('#statsBtn').click(function(){
        if(!$(this).hasClass('disabled')){
            stats.ajax.reload();
        }
    });

    $('input').on('change', function(){
        $("form :input[type='text']").each(function(){
            $('button[type="submit"]').removeClass('disabled').attr('disabled', false);
            if(!$(this).val()){
                $('button[type="submit"]').addClass('disabled').attr('disabled', true);
            }
        });
    });

    $('.update-site').click(function(){
        var values = {};
        values['siteid'] = $(this).val();
        values['site_name'] = $(this).parent().parent().find($('input[name=update-site]')).val();
        values['update-apikey'] = $(this).parent().parent().find($('input[name=update-apikey]')).val();
        values['update-site'] = 1;
        $.ajax({
            url: "index.php",
            type: "POST",
            data: values,
            success: function(response){
            }
        })

    });

    $('.remove-site').click(function(){
        var remsite = $(this).val();
        var thistr = $(this);
        $.ajax({
            url: "index.php",
            type: "POST",
            data: {remsite:remsite},
            success: function(response){
                if(response == 1){
                    thistr.closest("tr").remove();
                }
            }
        })
    });


});