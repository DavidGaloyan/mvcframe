<?php


class User extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->table = 'users';
    }

    public function login($username, $password)
    {

        $user = $this->db->queryOne($this->db->prepare("SELECT * FROM $this->table WHERE username = '%s' AND password = '%s'", $username, $password));
        if (!empty($user)) {
            Session::put('user', $user['id']);
            return true;
        }
        return false;
    }

}

