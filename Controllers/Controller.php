<?php

class  Controller extends easyDb
{
    protected $db;
    protected $table;

    public function __construct()
    {
        $this->db= DB::getInstance();
    }

    public function get()
    {
        return $this->db->queryArray("SELECT * FROM $this->table");
    }

    public function getByID($id)
    {
        return $this->db->queryOne("SELECT * FROM $this->table WHERE userid='$id'");
    }

    public function deleteByID($id)
    {
        return $this->db->query("DELETE FROM $this->table WHERE id = $id");
    }

    public static function all()
    {
       return (new self)->get();
    }

    public static function find($id)
    {
        return (new self)->getByID($id);
    }

    public static function remove($id)
    {
        return (new self)->deleteByID($id);
    }


}